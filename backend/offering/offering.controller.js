import { model as Offering } from './offering.model.js';

async function getOfferings(request, response) {
  await Offering.find()
    .exec()
    .then((offerings) => {
      response.json(offerings);
    });
}

async function createOffering(request, response) {
  const newOffering = request.body;
  await Offering.create({
    vendor: newOffering.vendor,
    price: newOffering.price,
  }).then((savedOffering) => {
    response.json(savedOffering);
  });
}

async function updateOffering(request, response) {
  const offeringId = request.params.id;
  await Offering.findById(offeringId)
    .exec()
    .then((offering) => {
      if (!offering) {
        return Promise.reject({
          message: `Offering with id ${offeringId} not found.`,
          status: 404,
        });
      }
      const providedOffering = request.body;
      offering.vendor = providedOffering.vendor;
      offering.price = providedOffering.price;
      return offering.save();
    })
    .then((updatedOffering) => {
      response.json(updatedOffering);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

async function removeOffering(request, response) {
  const offeringId = request.params.id;
  await Offering.findById(offeringId)
    .exec()
    .then((offering) => {
      if (!offering) {
        return Promise.reject({
          message: `Offering with id ${offeringId} not found.`,
          status: 404,
        });
      }
      return offering.deleteOne();
    })
    .then((deletedOffering) => {
      response.json(deletedOffering);
    })
    .catch((error) => {
      if (error.status) {
        response.status(error.status);
      } else {
        response.status(500);
      }
      response.json({ message: error.message });
    });
}

export { getOfferings, createOffering, updateOffering, removeOffering };
