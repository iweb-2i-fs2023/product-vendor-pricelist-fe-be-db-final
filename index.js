import express from 'express';
import { router as offeringRouter } from './backend/offering/offering.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect('mongodb://localhost/bookofferings');

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/offerings', offeringRouter);

mongoose.connection.once('open', () => {
  console.log('Connected to MongoDB');
  app.listen(3001, () => {
    console.log('Server listens to http://localhost:3001');
  });
});
